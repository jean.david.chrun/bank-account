package bankaccount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import bankaccount.exception.InsufficientAmountException;
import bankaccount.exception.InvalidAccountException;
import bankaccount.exception.InvalidAmountException;
import bankaccount.model.Transaction;
import bankaccount.model.TransactionType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@DisplayName("Bank Operation")
@ExtendWith(MockitoExtension.class)
class BankOperationServiceShould {

  @Mock
  private TransactionRepository transactionRepo;
  @Mock
  private PrintStream printStream;
  private BankOperationService bankOperationService;
  private Clock clock;

  @BeforeEach
  void setup() {
    this.clock = Clock.fixed(Instant.parse("2021-05-31T10:15:30.00Z"), ZoneId.of("UTC"));
    this.bankOperationService = new BankOperationServiceImpl(transactionRepo, clock);
  }


  @Test
  @DisplayName("Invalid deposit with ID Null, should throw InvalidAccountException")
  public void blockAnInvalidDepositWithNullId() {
    final var expectedMessage = "Please provide a not null and not empty account Id.";
    final var exception = Assertions.assertThrows(InvalidAccountException.class,
        () -> this.bankOperationService.deposit(null, BigDecimal.ONE));
    assertEquals(expectedMessage, exception.getMessage());
    verifyNoInteractions(transactionRepo);
  }

  @DisplayName("Invalid deposit with incorrect Amount, should throw InvalidAmountException")
  @ParameterizedTest(name = "{0}")
  @ValueSource(ints = {
      0,
      -5
  })
  public void blockAnInvalidDepositWithIncorrectAmount(int amount) throws InvalidAccountException {
    final var accountId = "40";
    when(transactionRepo.findLast(accountId)).thenReturn(Optional.empty());
    final var expectedMessage = "Please provide a not null and positive deposit.";
    final var exception =
        Assertions.assertThrows(InvalidAmountException.class,
            () -> this.bankOperationService.deposit(accountId, BigDecimal.valueOf(amount)));

    assertEquals(expectedMessage, exception.getMessage());
  }


  @Test
  @DisplayName("Accept a valid deposit")
  public void acceptAValidDeposit() throws InvalidAmountException, InvalidAccountException {
    final var accountId = "40";
    final var lastBalance = BigDecimal.valueOf(200);
    final var lastAmount = BigDecimal.valueOf(100);
    final var transaction = Optional.of(Transaction
        .createTransaction(accountId, LocalDate.now(clock), TransactionType.DEPOSIT,
            lastBalance, lastAmount));

    when(transactionRepo.findLast(accountId)).thenReturn(transaction);

    final var amountToDeposit = BigDecimal.valueOf(100);
    final var expectedBalance = lastBalance.add(amountToDeposit)
        .setScale(2, RoundingMode.HALF_EVEN);
    final var actualBalance = bankOperationService.deposit(accountId, amountToDeposit)
        .getBalance();

    assertEquals(expectedBalance, actualBalance);
    verify(transactionRepo, times(1)).findLast(accountId);
  }

  @Test
  @DisplayName("Invalid withdrawal with ID Null, should throw InvalidAccountException")
  public void blockAnInvalidWithdrawalWithNullId() {
    final var expectedMessage = "Please provide a not null and not empty account Id.";
    final var exception =
        Assertions.assertThrows(InvalidAccountException.class,
            () -> this.bankOperationService.withdraw(null, BigDecimal.ONE));
    assertEquals(expectedMessage, exception.getMessage());
    verifyNoInteractions(transactionRepo);
  }

  @Test
  @DisplayName("Invalid withdrawal with greater amount, should throw InsufficientAmountException")
  public void blockAnInvalidWithdrawalWithTooMuchAmount()
      throws InvalidAmountException, InvalidAccountException {
    final var expectedMessage = "Please provide a not null and amount lower than 100.00 for a withdrawal.";
    final var accountId = "40";
    final var transaction =
        Transaction.createTransaction(accountId,
            LocalDate.now(), TransactionType.DEPOSIT, BigDecimal.valueOf(100),
            BigDecimal.valueOf(100));

    when(transactionRepo.findLast(accountId)).thenReturn(Optional.of(transaction));

    final var exception =
        Assertions.assertThrows(InsufficientAmountException.class,
            () -> this.bankOperationService.withdraw(accountId, BigDecimal.valueOf(10000)));
    assertEquals(expectedMessage, exception.getMessage());
    verify(transactionRepo, times(1)).findLast(accountId);
    verifyNoMoreInteractions(transactionRepo);
  }

  @Test
  @DisplayName("Accept a valid Withdraw")
  public void acceptAValidWithdrawal() throws InvalidAmountException, InvalidAccountException,
      InsufficientAmountException {
    final var accountId = "40";
    final var balance = BigDecimal.valueOf(115);
    final var amount = BigDecimal.valueOf(115);
    final var transaction =
        Optional.of(Transaction
            .createTransaction(accountId, LocalDate.now(clock), TransactionType.DEPOSIT, balance,
                amount));
    when(transactionRepo.findLast(accountId)).thenReturn(transaction);

    final var actualBalance = bankOperationService.withdraw(accountId, BigDecimal.valueOf(100))
        .getBalance();

    final var expectedBalance = BigDecimal.valueOf(15).setScale(2, RoundingMode.HALF_EVEN);

    assertEquals(expectedBalance, actualBalance);
  }

  @Test
  @DisplayName("Print Statement with a null PrintStream, should throw IOException")
  public void blockAnInvalidPrintStatementWithNullPrintStream() {
    Assertions.assertThrows(NullPointerException.class,
        () -> this.bankOperationService.printStatement("40", null));
    verifyNoInteractions(transactionRepo);
  }

  @Test
  @DisplayName("Print Statement with an invalid accountId, should throw InvalidAccountException")
  public void blockAnInvalidPrintStatementWithInvalidAccountId() {
    final var expectedMessage = "Please provide a not null and not empty account Id.";
    final var exception =
        Assertions.assertThrows(InvalidAccountException.class,
            () -> this.bankOperationService.printStatement(null, System.out));
    assertEquals(expectedMessage, exception.getMessage());
    verifyNoInteractions(transactionRepo);
  }

  @Test
  @DisplayName("Print Statement with no transaction")
  public void printStatementWithNoTransaction() throws IOException, InvalidAccountException {
    final var accountId = "40";
    final var expectedStatement = "No historic found for id 40\n";
    final var out = new ByteArrayOutputStream();
    final var printStream = new PrintStream(out);

    this.bankOperationService.printStatement(accountId, printStream);

    assertEquals(expectedStatement, out.toString());
    verify(transactionRepo, times(1)).findAll(accountId);
    verifyNoMoreInteractions(transactionRepo);
  }

  @Test
  @DisplayName("Print Statement with transactions")
  public void printStatementWithTransactions()
      throws IOException, InvalidAccountException, InvalidAmountException {

    final var accountId = "40";
    final var dateT1 = LocalDate.now(clock).minusDays(1);
    final var dateT2 = LocalDate.now(clock);
    final var amountToDeposit = BigDecimal.valueOf(150).setScale(2);
    final var balanceT1 = BigDecimal.valueOf(1150).setScale(2);
    final var balanceT2 = BigDecimal.valueOf(1300).setScale(2);
    final var transaction1 = Transaction
        .createTransaction(accountId, dateT1, TransactionType.DEPOSIT, balanceT1, amountToDeposit);
    final var transaction2 = Transaction
        .createTransaction(accountId, dateT2, TransactionType.DEPOSIT, balanceT2, amountToDeposit);

    when(transactionRepo.findAll(accountId)).thenReturn(List.of(transaction1, transaction2));

    final var expectedStatement =
        """
            account 40
             | DEPOSIT | 2021-05-30 | 150.00 | 1150.00 |\s
             | DEPOSIT | 2021-05-31 | 150.00 | 1300.00 |\s
            """;
    this.bankOperationService.printStatement(accountId, printStream);

    verifyNoMoreInteractions(transactionRepo);
    final var inOrder = Mockito.inOrder(transactionRepo, printStream);
    inOrder.verify(transactionRepo, times(1)).findAll(accountId);
    inOrder.verify(printStream, times(1)).println(expectedStatement);

  }

}