package bankaccount.model;

import bankaccount.exception.InvalidAccountException;
import bankaccount.exception.InvalidAmountException;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


public class TransactionShould {


  @DisplayName("Invalid creation of Transaction with Null account id")
  @Test
  public void throwsInvalidAccountErrorWithNullAccountId() {
    final var expectedMessage = "Please provide a not null and not empty account Id.";
    final var exception = Assertions.assertThrows(InvalidAccountException.class,
        () -> Transaction
            .createTransaction(null, LocalDate.now(), TransactionType.DEPOSIT, BigDecimal.ONE,
                BigDecimal.ONE));
    Assertions.assertEquals(expectedMessage, exception.getMessage());
  }

  @DisplayName("Invalid creation of Transaction with Only Spaces account id")
  @Test
  public void throwsInvalidAccountErrorWithOnlySpacesAccountId() {
    final var expectedMessage = "Please provide a not null and not empty account Id.";
    final var exception = Assertions.assertThrows(InvalidAccountException.class,
        () -> Transaction.createTransaction(" ", LocalDate.now(),
            TransactionType.DEPOSIT, BigDecimal.ONE, BigDecimal.ONE));
    Assertions.assertEquals(expectedMessage, exception.getMessage());
  }

  @DisplayName("Invalid creation of Transaction with Invalid amount")
  @ParameterizedTest(name = "amount : {0}")
  @ValueSource(strings = {
      "0",
      "-5"
  })
  public void throwsInvalidAmountErrorWithInvalidAmount(String amount) {
    final var expectedMessage = "Please provide a not null and positive deposit.";
    final var exception = Assertions.assertThrows(InvalidAmountException.class,
        () -> Transaction
            .createTransaction("40", LocalDate.now(), TransactionType.DEPOSIT, BigDecimal.ONE,
                BigDecimal.valueOf(Integer.valueOf(amount))));
    Assertions.assertEquals(expectedMessage, exception.getMessage());
  }

  @Test
  public void createValidTransaction() throws InvalidAmountException, InvalidAccountException {
    var expectedDate = LocalDate.now();
    var expectedAccountId = "40";
    var expectedBalance = BigDecimal.ONE.setScale(2);
    var expectedAmount = BigDecimal.valueOf(100).setScale(2);
    var actualTransaction = Transaction
        .createTransaction(expectedAccountId, expectedDate, TransactionType.DEPOSIT,
            expectedBalance, expectedAmount);

    Assertions.assertEquals(expectedAccountId, actualTransaction.getAccountId());
    Assertions.assertEquals(expectedDate, actualTransaction.getDate());
    Assertions.assertEquals(expectedAmount, actualTransaction.getAmount());
    Assertions.assertEquals(expectedBalance, actualTransaction.getBalance());
  }
}
