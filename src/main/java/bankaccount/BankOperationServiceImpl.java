package bankaccount;


import bankaccount.exception.InsufficientAmountException;
import bankaccount.exception.InvalidAccountException;
import bankaccount.exception.InvalidAmountException;
import bankaccount.model.Transaction;
import bankaccount.model.TransactionType;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class BankOperationServiceImpl implements BankOperationService {

  private final TransactionRepository transactionRepo;
  private final Clock clock;
  private final static String DELIMITER_FORMAT = " | ";
  private final static String NEW_LINE_FORMAT = System.lineSeparator();

  public BankOperationServiceImpl(TransactionRepository transactionRepo, Clock clock) {
    this.transactionRepo = transactionRepo;
    this.clock = clock;
  }

  @Override
  public Transaction deposit(String accountId, BigDecimal amount)
      throws InvalidAmountException, InvalidAccountException {

    final var lastBalance = getBalance(accountId);
    final var newBalance = lastBalance.add(amount);

    final var transaction = Transaction
        .createTransaction(accountId, LocalDate.now(clock), TransactionType.DEPOSIT, newBalance,
            amount);

    transactionRepo.add(transaction);

    return transaction;
  }


  @Override
  public Transaction withdraw(String accountId, BigDecimal amount)
      throws InvalidAmountException, InvalidAccountException, InsufficientAmountException {
    final var lastBalance = getBalance(accountId);
    if (amount == null || amount.compareTo(lastBalance) > 0) {
      throw new InsufficientAmountException(
          "Please provide a not null and amount lower than " + lastBalance + " for a withdrawal.");
    }

    final var newBalance = lastBalance.subtract(amount);
    final var transaction = Transaction
        .createTransaction(accountId, LocalDate.now(clock), TransactionType.WITHDRAWAL, newBalance,
            amount);

    transactionRepo.add(transaction);

    return transaction;
  }

  private BigDecimal getBalance(String accountId) throws InvalidAccountException {
    if (accountId == null || accountId.isBlank()) {
      throw new InvalidAccountException("Please provide a not null and not empty account Id.");
    }
    return transactionRepo.findLast(accountId).map(Transaction::getBalance).orElse(BigDecimal.ZERO);
  }

  @Override
  public void printStatement(String accountId, PrintStream printStream)
      throws InvalidAccountException {
    if (accountId == null || accountId.isBlank()) {
      throw new InvalidAccountException("Please provide a not null and not empty account Id.");
    }
    Objects.requireNonNull(printStream);
    final var transactions = transactionRepo.findAll(accountId);
    final var formatToPrint = formatStatement(accountId, transactions);

    printStream.println(formatToPrint);
  }

  private String formatStatement(String accountId, List<Transaction> transactions) {
    if (transactions == null || transactions.isEmpty()) {
      return "No historic found for id " + accountId;
    }
    var sb = new StringBuilder();
    sb.append("account ").append(accountId).append(NEW_LINE_FORMAT);
    for (Transaction transaction : transactions) {
      sb.append(formatOneTransaction(transaction));
    }
    return sb.toString();
  }

  private String formatOneTransaction(Transaction transaction) {
    var sb = new StringBuilder();
    sb.append(DELIMITER_FORMAT).append(transaction.getTransactionType()).append(DELIMITER_FORMAT)
        .append(transaction.getDate()).append(DELIMITER_FORMAT).append(transaction.getAmount())
        .append(DELIMITER_FORMAT).append(transaction.getBalance()).append(DELIMITER_FORMAT)
        .append(NEW_LINE_FORMAT);
    return sb.toString();
  }

}
