package bankaccount.model;

import bankaccount.exception.InvalidAccountException;
import bankaccount.exception.InvalidAmountException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

public class Transaction {

  private final String accountId;
  private final LocalDate date;
  private final TransactionType transactionType;
  private final BigDecimal balance;
  private final BigDecimal amount;

  private Transaction(String accountId, LocalDate date, TransactionType transactionType,
      BigDecimal balance, BigDecimal amount) {
    this.accountId = accountId;
    this.date = date;
    this.transactionType = transactionType;
    this.balance = balance;
    this.amount = amount;
  }

  public static Transaction createTransaction(String accountId, LocalDate date,
      TransactionType transactionType, BigDecimal balance, BigDecimal amount)
      throws InvalidAccountException, InvalidAmountException {
    if (null == accountId || accountId.isBlank()) {
      throw new InvalidAccountException("Please provide a not null and not empty account Id.");
    }
    if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
      throw new InvalidAmountException("Please provide a not null and positive deposit.");
    }

    final var scaledBalance = balance.setScale(2, RoundingMode.HALF_EVEN);
    final var scaledAmount = amount.setScale(2, RoundingMode.HALF_EVEN);

    return new Transaction(accountId, date, transactionType, scaledBalance, scaledAmount);
  }

  public String getAccountId() {
    return accountId;
  }

  public LocalDate getDate() {
    return date;
  }

  public TransactionType getTransactionType() {
    return transactionType;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  @Override
  public boolean equals(Object o) {
      if (this == o) {
          return true;
      }
      if (o == null || getClass() != o.getClass()) {
          return false;
      }
    Transaction that = (Transaction) o;
    return Objects.equals(accountId, that.accountId) && Objects.equals(date, that.date)
        && transactionType == that.transactionType && Objects.equals(balance, that.balance)
        && Objects.equals(amount, that.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, date, transactionType, balance, amount);
  }

}
