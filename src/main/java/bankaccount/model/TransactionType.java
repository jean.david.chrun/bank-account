package bankaccount.model;

public enum TransactionType {
  DEPOSIT, WITHDRAWAL
}
