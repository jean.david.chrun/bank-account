package bankaccount;

import bankaccount.exception.InsufficientAmountException;
import bankaccount.exception.InvalidAccountException;
import bankaccount.exception.InvalidAmountException;
import bankaccount.model.Transaction;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;

public interface BankOperationService {

  Transaction deposit(String accountId, BigDecimal amount)
      throws InvalidAmountException, InvalidAccountException;

  Transaction withdraw(String accountId, BigDecimal amount)
      throws InvalidAmountException, InvalidAccountException, InsufficientAmountException;

  void printStatement(String accountId, PrintStream printStream)
      throws InvalidAccountException, IOException;

}
