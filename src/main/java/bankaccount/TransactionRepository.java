package bankaccount;

import bankaccount.exception.InvalidAccountException;
import bankaccount.model.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository {

  void add(Transaction transaction) throws InvalidAccountException;

  Optional<Transaction> findLast(String accountId) throws InvalidAccountException;

  List<Transaction> findAll(String accountId) throws InvalidAccountException;
}
